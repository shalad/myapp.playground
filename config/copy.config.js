module.exports = {
  copyMobiscrollCss: {
    src: [
      '{{ROOT}}/node_modules/@mobiscroll/angular/dist/css/*',
      '{{ROOT}}/node_modules/@mobiscroll/angular-lite/dist/css/*'
    ],
    dest: '{{WWW}}/lib/mobiscroll/css/'
  },
  "copyMaterialIcons": {
    "src": [
      "{{ROOT}}/node_modules/ionic2-material-icons/fonts/**/*"
    ],
    "dest": "{{WWW}}/assets/fonts"
  },
  copyFirebaseSW: {
    src: [
      '{{ROOT}}/src/firebase-messaging-sw.js'
    ],
    dest: '{{WWW}}'
  },
  "copyWorkboxSw": {
    "src": [
      "{{ROOT}}/node_modules/workbox-sw/build/workbox-sw.js",
      "{{ROOT}}/node_modules/workbox-core/build/workbox-core.prod.js",
      "{{ROOT}}/node_modules/workbox-precaching/build/workbox-precaching.prod.js",
      "{{ROOT}}/node_modules/workbox-routing/build/workbox-routing.prod.js",
      "{{ROOT}}/node_modules/workbox-strategies/build/workbox-strategies.prod.js"
    ],
    "dest": "{{WWW}}/workbox"
  }
}
