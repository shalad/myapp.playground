#!/usr/bin/env node

// this plugin replaces arbitrary text in arbitrary files
/*
  Create /config/cordova.replace.config.json";
{
  "replacements": [
    {
      "file": "file to replace the strings",
      "search": "regex to find",
      "replace": "replacement string"
    }
  ]
}
 */
//

const fs = require('fs');
const path = require('path');
const rootdir = process.argv[2];

function replace_string_in_file(filename, to_replace, replace_with) {
  var data = fs.readFileSync(filename, 'utf8');
  var result = data.replace(new RegExp(to_replace, "g"), replace_with);

  fs.writeFileSync(filename, result, 'utf8');
}

if (rootdir) {
  const configFile = path.join(rootdir, "config", "cordova.replace.config.json");
  const configObj = JSON.parse(fs.readFileSync(configFile, 'utf8'));

  if (configObj && configObj.replacements) {
    configObj.replacements.forEach(function(replacementObj, index, array) {
      let fullfilename = path.join(rootdir, replacementObj.file);

      if (fs.existsSync(fullfilename)) {
        // CONFIGURE HERE
        replace_string_in_file(fullfilename, replacementObj.search, replacementObj.replace);
      }
    });
  }
}
