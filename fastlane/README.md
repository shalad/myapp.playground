fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios build_debug
```
fastlane ios build_debug
```
Build Debug
### ios build_device
```
fastlane ios build_device
```
Build to device
### ios build_release
```
fastlane ios build_release
```
Build Release

----

## Android
### android build_debug
```
fastlane android build_debug
```
Build Debug
### android build_release
```
fastlane android build_release
```
Build Release
### android release
```
fastlane android release
```
Release with Script
### android build_and_sign
```
fastlane android build_and_sign
```
Build Release and sign without uploading

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
