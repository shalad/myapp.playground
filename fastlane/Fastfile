# First of all
# fastlane deliver init
# fastlane supply init
default_platform(:ios)

configFile = File.read('../fastlaneConfig/config.json')
config = JSON.parse(configFile)

def replace_in_file(filename, to_replace, replace_with)
  puts filename
  puts to_replace
  puts replace_with
  if (File.file?(filename))
    text = File.read(filename)
    File.write(filename, text.gsub(/to_replace/, replace_with))
  end
end

def copyConfigXML(platform)
  if (File.file?('../fastlaneConfig/config_'+ platform +'.xml'))
    UI.message('A custom config.xml File exists for platform '+ platform +'. Copy it.');
    copy_files(source: 'fastlaneConfig/config_'+ platform +'.xml', destination: 'config.xml')
  end
end

def enableGenericVersioning(config, version)
  UI.important "Switching to Apple Generic Versioning"
  fastlane_require 'xcodeproj'
  project = Xcodeproj::Project.open("../platforms/ios/"+ config["packageName"] +".xcodeproj")

  target = project.native_targets.detect { |target| target.name == config["packageName"] }

  target.build_configurations.each do |item|
    UI.message(item)
    item.build_settings['CURRENT_PROJECT_VERSION'] = version
    item.build_settings['VERSIONING_SYSTEM'] = "Apple Generic"
  end
  project.save
end

platform :ios do
  before_all do
    ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"
  end

  desc "Build Debug"
  lane :build_debug do
    sh 'npm run copy:dev'
#     cert(
#       filename: 'Development',
#       development: true,
#       team_id: config['teamId'],
#       team_name: config['teamName']
#     )
    sigh(
      development: true,
      app_identifier: config['appIdentifierIos'],
      team_id: config['teamId'],
      team_name: config['teamName']
    )
    copyConfigXML('ios')
    ionic(
      platform: 'ios',
      prod: true,
      release: false,
      type: 'development',
      cordova_build_config_file: 'fastlaneConfig/build.json'
    )
    appcenter_upload(
      api_token: "7f337564d8616609e31ab5321d3e4ca48f8ed128",
      owner_name: "andavis-apps",
      app_name: config["appCenterNameIOS"],
      ipa: "platforms/ios/build/device/"+ config["packageName"] +".ipa",
      destinations: config["distributeGroup"],
      destination_type: "group",
      notify_testers: false # Set to false if you don't want to notify testers of your new release (default: `false`)
    )
    slack(message: "Build "+ config["packageName"] +" Debug for Ios and uploaded to Appcenter")
  end

  desc "Build to device"
  lane :build_device do
    cert(
      development: true,
      team_id: config['teamId'],
      team_name: config['teamName']
    )
    sigh(
      development: true,
      app_identifier: config['appIdentifierIos'],
      team_id: config['teamId'],
      team_name: config['teamName']
    )
    copyConfigXML('ios')
    sh '../scripts/buildDebugIos.sh'
    sh "ios-deploy -b platforms/ios/build/device/"+ config["packageName"] +".ipa"
  end

  desc "Build Release"
  lane :build_release do
    sh 'npm run copy:prod'
    cert(
      team_id: config['teamId']
    )
    sigh(
      app_identifier: config['appIdentifierIos'],
      team_id: config['teamId']
    )
    copyConfigXML('ios')
    latestBuildNumber = latest_testflight_build_number(version: config['version'])

    newVersion = (latestBuildNumber.to_i + 1).to_s
    sh 'node replaceBuildNumber.js ../config.xml ios-CFBundleVersion=\"'+ newVersion +'\"'
    ionic(
      platform: 'ios',
      cordova_build_config_file: 'fastlaneConfig/build.json'
    )
    pilot(
      ipa: "platforms/ios/build/device/"+ config["packageName"] +".ipa",
      skip_waiting_for_build_processing: true
    )
    slack(message: "Build "+ config["packageName"] +" Release for Ios and uploaded to Testflight")
  end
end


platform :android do
  before_all do
    ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"
    #sh "export NODE_OPTIONS=--max-old-space-size=4096"
  end

  desc "Build Debug"
  lane :build_debug do
    sh 'npm run copy:dev'
    copyConfigXML('android')
    sh "ionic cordova build android"
    appcenter_upload(
      api_token: "7f337564d8616609e31ab5321d3e4ca48f8ed128",
      owner_name: "andavis-apps",
      app_name: config["appCenterNameAndroid"],
      apk: "platforms/android/app/build/outputs/apk/debug/app-debug.apk",
      destinations: config["distributeGroup"],
      destination_type: "group",
      notify_testers: false # Set to false if you don't want to notify testers of your new release (default: `false`)
    )
    slack(message: 'Build '+ config["packageName"] +' Debug for Android and uploaded to Appcenter')
  end

  desc "Build Release"
  lane :build_release do
    sh 'npm run copy:prod'
    copyConfigXML('android')
    version = google_play_track_version_codes(track: 'alpha')
    ionic(
      platform: 'android',
      keystore_path: 'fastlaneConfig/'+ config["keyStoreFileName"],
      keystore_password: config["keyStorePassword"],
      keystore_alias: config["keyStoreAlias"] || config["packageName"],
      build_number: version.last.to_i + 1,
      prod: true,
      release: true
    )
    upload_to_play_store(
      track: 'alpha',
      apk: 'platforms/android/app/build/outputs/apk/release/app-release.apk',
      package_name: config["appIdentifier"]
    )
    slack(message: "Build "+ config["packageName"] +" Release for Android and released to AlphaChannel")
  end

  desc "Release with Script"
  lane :release do
    copyConfigXML('android')
#     version = google_play_track_version_codes(track: 'alpha')
    newVersion = (config['versionCode'].to_i + 1).to_s
    sh 'node replaceVersionCode.js ../config.xml android-versionCode=\"'+ newVersion +'\"'
    sh '../scripts/buildReleaseApk.sh'
    upload_to_play_store(
      track: 'alpha',
      apk: 'platforms/android/app/build/outputs/apk/release/app-release.apk',
      package_name: config["appIdentifier"]
    )
    slack(message: "Build "+ config["packageName"] +" Release for Android and released to AlphaChannel")
  end

  desc "Build Release and sign without uploading"
  lane :build_and_sign do
    sh "ionic cordova build android --release"
    sh "xcopy /K /D /H /Y ..\\fastlaneConfig\\"+ config['keyStoreFileName'] +" ..\\platforms\\android\\app\\build\\outputs\\apk\\release\\"
    #sh "cd ..\\platforms\\android\\app\\build\\outputs\\apk\\release"
    sh "jarsigner -storepass "+ config['keyStorePassword'] +" -sigalg SHA1withRSA -digestalg SHA1 -keystore ..\\fastlaneConfig\\"+ config['keyStoreFileName'] +" ..\\platforms\\android\\app\\build\\outputs\\apk\\release\\app-release-unsigned.apk "+ config["keyStoreAlias"]
    sh "zipalign -f -v -p 4 ..\\platforms\\android\\app\\build\\outputs\\apk\\release\\app-release-unsigned.apk ..\\platforms\\android\\app\\build\\outputs\\apk\\release\\app-release.apk"
  end
end
