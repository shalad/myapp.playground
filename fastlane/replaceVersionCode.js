const fs = require('fs');
const path = require('path');
const rootDir = process.argv[2];

var file = process.argv[2];
var replace = process.argv[3];

if (replace) {
  replace_string_in_file(file, replace);
} else {
  console.error('File not exists or to few parameters');
  console.log('run `node replace.js FILE_NAME SEARCH REPLACE`')
}

function replace_string_in_file(filename, replace_with) {
  var data = fs.readFileSync(filename, 'utf8');

  var result = data.replace(/android-versionCode="([0-9]*)"/g, replace_with);

  fs.writeFileSync(filename, result, 'utf8');
  console.log('replaced');
}
